var net = require('net');

var connections = []; // connection's array to broadcast messages

// This function allow us to broadcast messages from client to client
var broadcast = function (message, origin) {
  connections.forEach(function (connection) {
    if (connection === origin) return;// if the message is from the same origin it will return to do not duplicate messages
    connection.write(message);
  });
};

net.createServer(function (connection) {
  connections.push(connection); // add a new connection to array of connections 
  connection.write('Hello, I am the server!');// send a welcome message to the clients
  connection.on('data', function (message) { // on data exchange it will call back a function which will return a message
    var command = message.toString();
    if (command.indexOf('/nickname') === 0) { // if the first letters of the message equak /nickname 
      var nickname = command.replace('/nickname ', '');// it will replace for the rest of the string
      broadcast(connection.nickname + ' is now ' + nickname);// and will broadcast for all participants 
      connection.nickname = nickname;
      return;
    }
    broadcast(connection.nickname + ' > ' + message, connection); // display who texted and the message to broadcast
  });
  connection.on('end', function () { // when a connection disconnects it will handle to do not break the server
    broadcast(connection.nickname + ' has left!', connection);
    connections.splice(connections.indexOf(connection), 1);
  });
}).listen(3000);// Listenning to port 3000 