var net = require('net');

var client = net.connect(3000);// connects to port 3000 where the server is listening 
client.on('connect', function () { // on event of connection 
	client.write('Hello, I am the client!');// this message is send to the server
});
client.on('data', function (message) { // on data exchange this print the message to the clients
	console.log(message.toString());
});
client.on('end', function () { // when the server stops, all clients are disconnected 
	process.exit();
});
process.stdin.on('readable', function () { // get the input from client and sends to server
	var message = process.stdin.read();
	if (!message) return;
	message = message.toString().replace(/\n/, ''); // replacing the enter
	client.write(message);
});